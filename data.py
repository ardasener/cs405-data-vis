import pandas as pd
import numpy as np

trip_data = pd.read_csv("data/yellow_tripdata_2019-06-min.csv", nrows=150000)
taxi_zones = pd.read_csv("data/taxi_zones.csv")

print("--- TRIP DATA ---")
print("Shape:", trip_data.shape)
print("Labels:", trip_data.keys())
print(trip_data.head())


print("--- TAXI ZONES ---")
print("Shape:", taxi_zones.shape)
print("Labels:", taxi_zones.keys())
print(taxi_zones.head())

