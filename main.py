import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go

from dash.dependencies import Input, Output
from datetime import datetime
from colour import Color

import pandas as pd
import numpy as np
import random


# To convert datetime string to integer
def to_integer(time_str):
    dt_time = datetime.strptime(time_str, '%Y-%m-%d %H:%M:%S')
    return 60*60*dt_time.hour + 60*dt_time.minute + dt_time.second

trip_data = pd.read_csv("data/yellow_tripdata_2019-06-min.csv", nrows=150000)
taxi_zones = pd.read_csv("data/taxi_zones.csv", dtype={'X': object, 'Y': object})


trip_data["int_time"] = trip_data["tpep_pickup_datetime"].map(to_integer)

print("--- TRIP DATA ---")
print("Shape:", trip_data.shape)
print("Labels:", trip_data.keys())
print(trip_data.head())

print("--- TAXI ZONES ---")
print("Shape:", taxi_zones.shape)
print("Labels:", taxi_zones.keys())
print(taxi_zones.head())

lons = taxi_zones['X'].tolist()
lats = taxi_zones['Y'].tolist()
zones = taxi_zones['zone'].tolist()

zone_counts = trip_data['PULocationID'].value_counts().to_dict()

marker_colors = []
for i in range(len(zones)):
    count = zone_counts.get(i+1,0)

    if count == 0:
        marker_colors.append("#DC3545")
    elif count < 20:
        marker_colors.append("#FFC107")
    else:
        marker_colors.append("#28A745")

external_stylesheets = [
    'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css',
    {
        'integrity':"sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh",
        'rel': 'stylesheet',
        'crossorigin': 'anonymous'
    },
]

mapbox_access_token = "pk.eyJ1IjoiYXJkYXNlbmVyIiwiYSI6ImNrNDQybGcybjA3MWEza3A5cmdqcnZqa3QifQ.6e7Ad-MArd5fv8tpvVRbyA"

app = dash.Dash(
    __name__,
    external_stylesheets=external_stylesheets
)

app.scripts.config.serve_locally = False
app.css.config.serve_locally = False

app.layout = html.Div(children = [
        html.Div(children=[
            html.Div(children=[
                html.Div(children=[
                    dcc.Graph(id="map-graph")
                ], className="col"),

                html.Div(children=[
                    html.H3(id="status-text", style={"margin-bottom":"5vh", "align-self":"center", "text-align":"center"}),
                    html.Label(htmlFor="pick-up-select", children="Select Pick Up Location:", style={"color":"#006ADB"}),
                    dcc.Dropdown(id="pick-up-select", options=[
                        {'label': zones[i-1], 'value': i} for i in range(1,len(zones)+1)
                    ], style={"margin-bottom":"5vh"}, value=43, clearable=False),
                    html.Label(htmlFor="time-selection-div", children="Select Pick Up Time:", style={"color":"#006ADB"}),
                    html.Div(className="row",id="time-selection-div", children=[
                        dcc.Dropdown(className="col", id="start-time-select", value=0, clearable=False, searchable=False, options=[
                            {'label': '{}:00'.format(i), 'value': i*60*60} for i in range(0,24)
                        ], style={"flex":1}),
                        html.H3(children="-"),
                        dcc.Dropdown(className="col", id="end-time-select", value=23*60*60, clearable=False, searchable=False, options=[
                            {'label': '{}:00'.format(i), 'value': i*60*60} for i in range(0,24)
                        ], style={"flex":1})
                    ])
                ], className="col"),
            ], className="row")
        ], className="container-fluid", style={"padding-top":"2vh", "width":"80%"}),

        html.Div(children=[
            html.Div(children=[
                html.Div(children=[
                    dcc.Graph(id="payment-type")
                ], className="col-3"),


                html.Div(children=[
                    dcc.Graph(id="tip-dist")
                ], className="col-3"),


                html.Div(children=[
                    dcc.Graph(id="location-bar")
                ], className="col-3"),
                

            ], className="row", style={"justify-content": "center"}),
        
        html.Div(children=[html.P(children="created by Arda Sener")],style={"text-align": "center"})
        
        ], className="container-fluid"),

    ])


def get_data(pick_up_location_id,start,end):
    data = trip_data[(trip_data["PULocationID"] == pick_up_location_id) & (trip_data["int_time"] < end) & (trip_data["int_time"] > start)]
    return data



@app.callback(Output("status-text", "children"), [Input("pick-up-select", "value"), Input("start-time-select", "value"), Input("end-time-select", "value")])
def update_status(pick_up_location_id,start,end):
    data = get_data(pick_up_location_id,start,end)
    if(data.shape[0] == 0):
        return html.Span(children="Insufficient Data", className="badge badge-danger")
    if(data.shape[0] < 20):
        return html.Span(children="Low Data", className="badge badge-warning")
    else:
        return html.Span(children="Sufficient Data", className="badge badge-success")


@app.callback(Output("payment-type", "figure"), [Input("pick-up-select", "value"), Input("start-time-select", "value"), Input("end-time-select", "value")])
def update_payment_type(pick_up_location_id,start,end):

    data = get_data(pick_up_location_id,start,end)

    payment_data = data["payment_type"].value_counts()

    #print(payment_data)
    values = payment_data.tolist()


    colors=["#006ADB", "#FFC107","#343A40", "#17A2B8","#28A745","#DC3545"]
    all_labels = ["Credit Card", "Cash", "No Charge", "Dispute", "Unknown", "Voided Trip"]
    labels = []

    for i in range(0,6):
        if i+1 in payment_data.keys():
            labels.append(all_labels[i])

    fig = go.Figure(go.Pie(values=values, labels=labels, hole=0.4, marker=dict(colors=colors)))
    fig.update_layout(title="Payment Methods")
    return fig

@app.callback(Output("tip-dist", "figure"), [Input("pick-up-select", "value"), Input("start-time-select", "value"), Input("end-time-select", "value")])
def update_tip_dist(pick_up_location_id,start,end):

    data = get_data(pick_up_location_id,start,end)

    fig = go.Figure(data = go.Scatter(x=data["trip_distance"],y=data["tip_amount"], mode='markers', marker=dict(color="#006ADB")))
    fig.update_layout(title="Tip Amount for Distance Traveled")

    return fig


def id_to_zone(id):

    if id < len(zones) and id > 0:
        return zones[id-1]
    else:
        return "Unknown Location"


def get_color_gradient(color1,color2,n):
    first = Color(color1)
    second = Color(color2)
    return list(first.range_to(second,n))

@app.callback(Output("pick-up-select", "value"), [Input("location-bar","clickData"), Input("map-graph","clickData")])
def update_from_bar(bar_click, map_click):

    ctx = dash.callback_context

    if not ctx.triggered:
        return 43
    elif ctx.triggered[0]['prop_id'] == "map-graph.clickData":

        if map_click == None:
            return 43

        location = map_click["points"][0]["text"]
        return zones.index(location) + 1
    else:

        if bar_click == None:
            return 43

        location = bar_click["points"][0]["x"]
        return zones.index(location) + 1



@app.callback(Output("location-bar", "figure"), [Input("pick-up-select", "value"), Input("start-time-select", "value"), Input("end-time-select", "value")])
def update_location_bar(pick_up_location_id,start,end):

    data = get_data(pick_up_location_id,start,end)

    location_data = data["DOLocationID"].value_counts().head(10)
    location_data = pd.DataFrame({'index':location_data.index, 'count':location_data.values})

    location_data["zone"] = location_data['index'].map(id_to_zone)
    
    #colors = ["#28A745","#DC3545","#006ADB", "#FFC107","#343A40", "#17A2B8", "#A7288A", "#FF9700", "#6E84FD", "#28A745"]

    colors = get_color_gradient("#28A745", "#DC3545",10)
    colors = list(map(lambda x: x.hex, colors))
    #print(location_data.head())

    fig = go.Figure(go.Bar(x=location_data['zone'], y=location_data['count'], marker_color=colors))
    fig.update_layout(title="Popular Drop Off Locations")
    return fig



@app.callback(Output("map-graph", "figure"), [Input("pick-up-select", "value")])
def update_graph(pick_up_location_id):

    pickup_index = pick_up_location_id - 1
    
    final_colors = marker_colors.copy()
    final_colors[pickup_index] = "#006ADB"


    fig = go.Figure(go.Scattermapbox(
            lat=taxi_zones['Y'],
            lon=taxi_zones['X'],
            mode='markers+text',
            marker=go.scattermapbox.Marker(
                size=12,
                ),
            marker_color=final_colors,
            text=taxi_zones['zone'],
            textposition="bottom center",
        ))

    fig.update_layout(
        hovermode='closest',
        mapbox=go.layout.Mapbox(
            accesstoken=mapbox_access_token,
            bearing=0,
            center=go.layout.mapbox.Center(
                lat=float(lats[pickup_index]),
                lon=float(lons[pickup_index])
            ),
            
            pitch=0,
            zoom=13
        ),
        margin=go.layout.Margin(l=10, r=0, t=0, b=50),
    )

    return fig

if __name__ == '__main__':
    app.run_server(debug=True)
